/* 
 * The MIT License
 *
 * Copyright 2018 edelan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


var ctx = document.getElementById('myChart1');
var myChart1 = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: ["S01", "S02", "S03", "S04"],
        datasets: [{
                label: "Nb personnes",
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                borderColor: 'rgb(255, 99, 132)',
                borderWidth: 1,
                data: [2, 3, 0, 1]
            }]
    },

    // Configuration options go here
    options: {
        title: {
            display: true,
            text: 'Absences nombre de personnes'
        }
    }
});

var ctx = document.getElementById("myChart2");
var myChart2 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["S01", "S02", "S03", "S04"],
        datasets: [{
                label: 'heures totales',
                data: [32, 48, 0, 24],
                backgroundColor: 'rgba(54, 162, 235, 0.2)',                    
                borderColor: 'rgba(54, 162, 235, 1)',                    
                borderWidth: 1
            }]
    },
    options: {
        title: {
            display: true,
            text: 'Absences heures cumulées'
        },
        scales: {
            yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
        }
    }
});

var ctx = document.getElementById('myChart3');
var myChart3 = new Chart(ctx, {
    // The type of chart we want to create
    type: 'pie',

    // The data for our dataset
    data: {
        labels: ["Engagé", "Restant"],
        datasets: [{
                label: "Budget Avancement",
                backgroundColor: [
                    'rgba(255, 206, 86, 0.5)',
                    'rgba(75, 192, 192, 0.5)'
                ],
                data: [4500.00, 3200.00]
                
            }]
    },

    // Configuration options go here
    options: {
        title: {
            display: true,
            text: 'Budget des dépenses'
        },
        tooltips: {
            afterLabel: '€'
        }
    }
});

var ctx = document.getElementById('myChart4');
var myChart4 = new Chart(ctx, {
    // The type of chart we want to create
    type: 'pie',

    // The data for our dataset
    data: {
        labels: ["Commercial", "Administratif", "Technique"],
        datasets: [{
                label: "Budget alloués",
                backgroundColor: [
                    'rgba(103, 58, 183, 0.5)',
                    'rgba(245, 230, 5, 0.5)',
                    'rgba(121, 85, 72, 0.5)'
                ],
                data: [2500.00, 2000.00, 3200.00]
                
            }]
    },

    // Configuration options go here
    options: {
        title: {
            display: true,
            text: 'Répartition par services'
        },
        tooltips: {
            afterLabel: '€'
        }
    }
});

var ctx = document.getElementById('myChart5');
var myChart5 = new Chart(ctx, {
    // The type of chart we want to create
    type: 'doughnut',

    // The data for our dataset
    data: {
        labels: ["Engagé", "Restant"],
        datasets: [{
                label: "Congés Avancement",
                backgroundColor: [
                    'rgba(65, 244, 91, 0.5)',
                    'rgba(244, 65, 196, 0.5)'
                ],
                data: [450, 320]
                
            }]
    },

    // Configuration options go here
    options: {
        title: {
            display: true,
            text: 'Prévus 2017-2018'
        },
        tooltips: {
            afterLabel: '€'
        }
    }
});

var ctx = document.getElementById('myChart6');
var myChart6 = new Chart(ctx, {
    // The type of chart we want to create
    type: 'doughnut',

    // The data for our dataset
    data: {
        labels: ["Administratif", "Commercial", "Technique"],
        datasets: [{
                label: "Congés Avancement",
                backgroundColor: [
                    'rgba(244, 65, 65, 0.5)',
                    'rgba(244, 169, 65, 0.5)',
                    'rgba(65, 112, 244, 0.5)'                   
                    
                ],
                data: [450, 320, 100]
                
            }]
    },

    // Configuration options go here
    options: {
        title: {
            display: true,
            text: 'Restant à prendre'
        },
        tooltips: {
            afterLabel: '€'
        }
    }
});