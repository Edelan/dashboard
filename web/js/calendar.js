/* 
 * The MIT License
 *
 * Copyright 2018 edelan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

$(document).ready(function () {
    //var date = new Date();
    //console.log(date);
    //var d = date.getDate();
    //var m = date.getMonth();
    //var y = date.getFullYear();
    //var user_id = {{user.id}};
    //var events = {{calendar | raw}};
    $('#calendar').fullCalendar({
        locale: 'fr',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultView: 'agendaWeek',
        views: {

        },
        eventClick: function (event, jsEvent, view) {
            var start =
                $('#modalTitle').html(event.title);
            $('#modalBodyDesc').html(event.descrip);
            $('#modalBodyStart').html(moment(event.start._i).format('DD/MM/YY - HH:mm'));
            $('#modalBodyEnd').html(moment(event.end._i).format('DD/MM/YY - HH:mm'));
            $('#deleteConf').hide();
            $('#displayEvent').modal();
            $('#deleteEvent').click(function (e) {
                e.preventDefault();
                $('#deleteEvent').hide();
                $('#deleteConf').show();
                $('#deleteConf').click(function () {
                    $('#displayEvent').modal('hide');
                    $.ajax({
                            url: '/calendar/delete',
                            data: 'cal_id=' + event.id,
                            type: "POST"

                        })
                        .done(function (data) {
                            console.log('delete success');
                            $('#calendar').fullCalendar( 'refresh' );
                        })
                        .fail(function (request, error) {
                            console.log('failed' + error);
                        });
                    
                });
            });
        },
        //navLinks: true, // can click day/week names to navigate views
        selectable: true, // Allows a user to highlight multiple days or timeslots by clicking and dragging.
        selectHelper: true, // Whether to draw a "placeholder" event while the user is dragging.
        select: function (start, end, allDay) {
            //console.log(start.format());
            //console.log(end.format());
            var start = moment(start).format('YYYY-MM-DD HH:mm:ss');
            var end = moment(end).format('YYYY-MM-DD HH:mm:ss');
            //console.log('start '+start);
            //console.log('end '+end); 
            $("#formEvent").modal();
            $("#createEvent").submit(function (e) {
                e.preventDefault();
                $("#formEvent").modal('hide');
                //console.log('clique button');
                var title = $('#cal_title').val();
                var descrip = $('#cal_descrip').val();
                // $.fullCalendar.formatDate() depréciée;                        

                if (title) {

                    $.ajax({
                            url: '/calendar/create',
                            data: 'cal_title=' + title + '&cal_start=' + start + '&cal_end=' + end + '&cal_descrip=' + descrip + '&user_id=' + user_id,
                            type: "POST"

                        })
                        .done(function (data) {
                            console.log('Added Successfully');
                        })
                        .fail(function (request, error) {
                            console.log('failed' + error);
                        });
                    console.log("start " + start + " end " + end);
                    $('#calendar').fullCalendar('renderEvent', {
                            title: title,
                            start: start,
                            end: end,
                            allDay: allDay

                        },
                        true
                    );
                }
                $('#calendar').fullCalendar('unselect');
            });
        },
        editable: true, // rend l'event clickable
        eventLimit: true, // allow "more" link when too many events
        events: events,
        eventDrop: function (event, delta) {
            //console.log('event start' + event.start);
            console.log(event.id);
            var start = moment(event.start).format('YYYY-MM-DD HH:mm:ss');
            console.log('moment start ' + start);
            var end = moment(event.end).format('YYYY-MM-DD HH:mm:ss');
            $.ajax({
                    url: '/calendar/update',
                    data: 'cal_id=' + event.id + '&cal_title=' + event.title + '&cal_start=' + start + '&cal_end=' + end + '&cal_descrip=' + event.descrip + '&user_id=' + user_id,
                    type: "POST"
                })
                .done(function (data) {
                    console.log("Updated Successfully");
                })
                .fail(function (reqest, error) {
                    console.log('failed' + error);
                });
        },
        eventResize: function (event) {
            var start = moment(event.start).format('YYYY-MM-DD HH:mm:ss');
            var end = moment(event.end).format('YYYY-MM-DD HH:mm:ss');
            $.ajax({
                    url: '/calendar/update',
                    data: 'cal_id=' + event.id + '&cal_title=' + event.title + '&cal_start=' + start + '&cal_end=' + end + '&cal_descrip=' + event.descrip + '&user_id=' + user_id,
                    type: "POST"
                })
                .done(function (data) {
                    console.log("Updated Successfully");
                })
                .fail(function (reqest, error) {
                    console.log('failed' + error);
                });
        }

    });
});