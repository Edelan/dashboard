<?php

require_once __DIR__.'/../vendor/autoload.php';

// instanciation de l'application
$app = new Silex\Application();

require __DIR__ . '/../app/app.php';
require __DIR__ . './../app/routes.php';

$app->run();
