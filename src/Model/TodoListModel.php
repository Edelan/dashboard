<?php

/*
 * The MIT License
 *
 * Copyright 2018 Your Name <e.delanoy@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Model;

/**
 * Description of TodoListModel
 *
 * @author Your Name <e.delanoy@gmail.com>
 */
class TodoListModel {

    private $id;
    private $content;
    private $status;
    private $due;
    private $done;
    private $order;
    private $user_id;

    function getId() {
        return $this->id;
    }

    function getContent() {
        return $this->content;
    }

    function getStatus() {
        return $this->status;
    }

    function getDue() {
        return $this->due;
    }

    function getDone() {
        return $this->done;
    }

    function getOrder() {
        return $this->order;
    }

    function getUser_id() {
        return $this->user_id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setContent($content) {
        $this->content = $content;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setDue($due) {
        $this->due = $due;
    }

    function setDone($done) {
        $this->done = $done;
    }

    function setOrder($order) {
        $this->order = $order;
    }

    function setUser_id($user_id) {
        $this->user_id = $user_id;
    }

}
