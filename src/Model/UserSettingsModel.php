<?php

/*
 * The MIT License
 *
 * Copyright 2018 edelan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Model;

/**
 * Description of UserSettingsModel
 *
 * @author edelan
 */
class UserSettingsModel {
    
    private $serv_id;
    private $serv_name;
    private $role_id;
    private $role_name;
    private $job_id;
    private $job_name;
    private $contract_id;
    private $contract_name;
    
    function getServ_id() {
        return $this->serv_id;
    }

    function getServ_name() {
        return $this->serv_name;
    }

    function getRole_id() {
        return $this->role_id;
    }

    function getRole_name() {
        return $this->role_name;
    }

    function getJob_id() {
        return $this->job_id;
    }

    function getJob_name() {
        return $this->job_name;
    }

    function getContract_id() {
        return $this->contract_id;
    }

    function getContract_name() {
        return $this->contract_name;
    }

    function setServ_id($serv_id) {
        $this->serv_id = $serv_id;
    }

    function setServ_name($serv_name) {
        $this->serv_name = $serv_name;
    }

    function setRole_id($role_id) {
        $this->role_id = $role_id;
    }

    function setRole_name($role_name) {
        $this->role_name = $role_name;
    }

    function setJob_id($job_id) {
        $this->job_id = $job_id;
    }

    function setJob_name($job_name) {
        $this->job_name = $job_name;
    }

    function setContract_id($contract_id) {
        $this->contract_id = $contract_id;
    }

    function setContract_name($contract_name) {
        $this->contract_name = $contract_name;
    }


}
