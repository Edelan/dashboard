<?php

namespace App\Model;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Description of UserModel
 *
 * @author Edelan
 */
class UserModel implements UserInterface {

    /**
     * User id
     * @var integer
     */
    private $id;

    /**
     * User email
     * @var string
     */
    private $email;

    /**
     * User plain password used for encoding password.
     * Not save
     * @var string
     */
    private $plainPassword; // permet de refaire le hashage et de le comparer au $user_passwordEncoded

    /**
     * Role
     * Values: ROLE_USER, ROLE_MANAGER or ROLE_ADMIN
     * @var string
     */
    private $role;
    private $role_name;

    /**
     * String used for encoding password
     * @var string
     */
    private $salt;

    /**
     * Encoded password save in database
     * @var string
     */
    private $password;

    /**
     * User status
     * false: deactivated user
     * true: activated user 
     * @var bool
     */
    private $status;
    private $firstname;
    private $lastname;
    private $name;
    private $email_perso;
    private $phone;
    private $phone_perso;
    private $birthdate;
    private $photo;
    private $genre;
    private $civility;
    private $adress;
    private $city;
    private $country;
    private $postal;
    private $contract;
    private $enter_date;
    private $job;
    private $job_name;
    private $service;
    private $service_name;
    private $manager;
    private $manager_name;

    public function getId() {
        return $this->id;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPlainPassword() {
        return $this->plainPassword;
    }

    public function getRole() {
        return $this->role;
    }

    function getRole_name() {
        return $this->role_name;
    }

    function getEmail_perso() {
        return $this->email_perso;
    }

    function getPhone() {
        return $this->phone;
    }

    function getPhone_perso() {
        return $this->phone_perso;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setPlainPassword($plainPassword) {
        $this->plainPassword = $plainPassword;
    }

    /**
     * Récupération de le titre de role (ROLE_USER, ROLE_ADMIN, ROLE_MANAGER)
     * @param type $role
     */
    public function setRole($role) {
        $this->role = $role;
    }

    function setRole_name($role_name) {
        $this->role_name = $role_name;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    function getFirstname() {
        return $this->firstname;
    }

    function getLastname() {
        return $this->lastname;
    }

    function getEmailPerso() {
        return $this->emailPerso;
    }

    function getBirthdate() {
        return $this->birthdate;
    }

    function getPhoto() {
        return $this->photo;
    }

    function getGenre() {
        return $this->genre;
    }

    function getCivility() {
        return $this->civility;
    }

    function getAdress() {
        return $this->adress;
    }

    function getCity() {
        return $this->city;
    }

    function getCountry() {
        return $this->country;
    }

    function getPostal() {
        return $this->postal;
    }

    function getContract() {
        return $this->contract;
    }

    function getEnter_date() {
        return $this->enter_date;
    }

    
    function getJob() {
        return $this->job;
    }

    function getJob_name() {
        return $this->job_name;
    }

    function getService() {
        return $this->service;
    }

    function getService_name() {
        return $this->service_name;
    }

    function getManager() {
        return $this->manager;
    }

    function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    function setEmailPerso($emailPerso) {
        $this->emailPerso = $emailPerso;
    }

    function setBirthdate($birthdate) {
        $this->birthdate = $birthdate;
    }

    function setPhoto($photo) {
        $this->photo = $photo;
    }

    function setGenre($genre) {
        $this->genre = $genre;
    }

    function setCivility($civility) {
        $this->civility = $civility;
    }

    function setAdress($adress) {
        $this->adress = $adress;
    }

    function setCity($city) {
        $this->city = $city;
    }

    function setCountry($country) {
        $this->country = $country;
    }

    function setPostal($postal) {
        $this->postal = $postal;
    }

    function setJob($job) {
        $this->job = $job;
    }

    function setJob_name($job_name) {
        $this->job_name = $job_name;
    }

    function setService($service) {
        $this->service = $service;
    }

    function setService_name($service_name) {
        $this->service_name = $service_name;
    }

    function setManager($manager) {
        $this->manager = $manager;
    }

    function setName($firstname, $lastname) {

        $this->name = $firstname . ' ' . $lastname;
    }

    function setEmail_perso($email_perso) {
        $this->email_perso = $email_perso;
    }

    function setPhone($phone) {
        $this->phone = $phone;
    }

    function setPhone_perso($phone_perso) {
        $this->phone_perso = $phone_perso;
    }

    function setContract($contract) {
        $this->contract = $contract;
    }

    function setEnter_date($enter_date) {
        $this->enter_date = $enter_date;
    }
    
    function getName() {

        return $this->name;
    }

    function getManager_name() {
        return $this->manager_name;
    }

    function setManager_name($manager_name) {
        $this->manager_name = $manager_name;
    }

    public function eraseCredentials() {
        // no used
    }

    public function getPassword() {
        return $this->password;
    }

    public function setSalt($salt) {
        $this->salt = $salt;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getRoles() {
        return [$this->getRole()];
    }

    public function getSalt() {
        return $this->salt;
    }

    public function getUsername(): string {
        // no username use
    }

}
