<?php

/*
 * The MIT License
 *
 * Copyright 2018 edelan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Model;

/**
 * Description of CalendarModel
 *
 * @author edelan
 */
class CalendarModel {
    /**
     *
     * @var int 
     */
    private $id;
    /**
     *
     * @var type 
     */
    private $title;
    /**
     *
     * @var type 
     */
    private $start;
    /**
     *
     * @var type 
     */
    /**
     *
     * @var type 
     */
    private $end;
    /**
     *
     * @var type 
     */
    private $descrip;
    /**
     *
     * @var type 
     */
    private $user_id;
    
    function getId() {
        return $this->id;
    }

    function getTitle() {
        return $this->title;
    }

    function getStart() {
        return $this->start;
    }

    function getEnd() {
        return $this->end;
    }
    function getDescrip() {
        return $this->descrip;
    }

    function getUser_id() {
        return $this->user_id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setStart($start) {
        $this->start = $start;
    }

    function setEnd($end) {
        $this->end = $end;
    }
    
    function setDescrip($descrip) {
        $this->descrip = $descrip;
    }

    
    function setUser_id($user_id) {
        $this->user_id = $user_id;
    }


}
