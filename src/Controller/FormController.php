<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;



/**
 * Description of FormController
 *
 * @author edelan
 */
class FormController {
    
    public function loginAdminAction(Request $request, Application $app) {
        // some default data for when the form is displayed the first time
    
    return $app['twig']->render('admin/login.html.twig');

    }
}
