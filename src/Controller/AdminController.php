<?php

/*
 * The MIT License
 *
 * Copyright 2018 Your Name <e.delanoy@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Controller;

use App\Model\UserModel;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of AdminController
 *
 * @author Your Name <e.delanoy@gmail.com>
 */
class AdminController {
    
    public function loginAdmin(Application $app) {
        // some default data for when the form is displayed the first time
    
    return $app['twig']->render('admin/admin-login.html.twig');

    }
    
    public function loginAdminAction(Request $request, Application $app) {
        // récupération de la requete $_POST
        // vérification des données
        // si ok ROLE_ADMIN alors redirection vers dashboard
        $error = false;
        if ($request->isMethod('POST')) {
            $email = \trim($request->request->get('user_email'));
            $plainPassword = \trim($request->request->get('user_password'));

            $user = $app['dao.user']->findEmail($email);
            //var_dump($user->getStatus());

            if (empty($email)) {
                $app['session']->getFlashBag()->add("danger", "Veuillez rentrer une adresse email");
                $error = true;
            } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $app['session']->getFlashBag()->add("danger", "l'addresse email n'est pas valide");
                $error = true;
            } elseif (filter_var($email, FILTER_VALIDATE_EMAIL) && !$user) {
                $app['session']->getFlashBag()->add("danger", "l'addresse email n'existe pas en base.\n Veuillez retaper votre adresse email ou contacter votre manager.");
                $error = true;
            } elseif (filter_var($email, FILTER_VALIDATE_EMAIL) && $user->getStatus() == NULL) {
                
                $app['session']->getFlashBag()->add("danger", "Vous n'êtes pas administrateur");
                $error = true;
            } else {
                $error = false;
            }

            if (empty($plainPassword)) {
                $app['session']->getFlashBag()->add("danger", "le mot de passe doit être renseigné");
                $error = true;
            }

            if (!$error) {
                echo 'je suis passe';
                $password = $user->getPassword();
                $salt = $user->getSalt();
                // get the encoding type
                $encoder = $app['security.encoder.bcrypt'];
                // Match the password
                $bool = ($encoder->isPasswordValid($password, $plainPassword, $salt));
                //echo 'bool ' . $bool;                
                if ($bool) {
                    // je logue et j'enregistre en session le user                    

                    $app['session']->set('user', $user);
//                    
                    return $app->redirect('/dashboard');
                } else {
                    $app['session']->getFlashBag()->add("danger", "les mots de passe ne sont pas exacts");

                    return $app['twig']->render('admin/admin-login.html.twig');
                }
            }
            return $app['twig']->render('admin/admin-login.html.twig');
        }
        
    }
    
    public function dashboardAdminAction(Application $app) {
        return $app['twig']->render('pages/dashboard.html.twig');
    }
    
    public function usersAdminAction(Application $app) {
        $users = $app['dao.user']->findAll();
    return $app['twig']->render('admin/admin-users.html.twig', [ 'users' => $users, 'user' => $app['session']->get('user') ]);
    }
    
    public function adminGetUserId(int $id, Application $app) {

        $user = $app['dao.user']->find($id);
        
        $data = [
            'user_id' => $user->getId(),
            'user_firstname' => $user->getFirstname(),
            'user_lastname' => $user->getLastname(),
            'user_email' => $user->getEmail(),
            'role_id' => $user->getRole(),
            'user_status' => $user->getStatus()
        ];
        
        $result = json_encode($data);
        return $result;
    }
    
    public function adminUpdateUserById(int $id, Request $request, Application $app) {
        $user = $app['dao.user']->find($id);

        if ($request->isMethod('POST')) {
            // vérification des données simplement sur les inclusions
            // $user->setId($request->request->get('user_id'));
            $user->setEmail($request->request->get('user_email'));
            $user->setFirstname($request->request->get('user_firstname'));
            $user->setLastname($request->request->get('user_lastname'));
            $user->setRole($request->request->get('role_id'));
            $user->setStatus($request->request->get('user_status'));
            
            
            $app['dao.user']->adminSaveUser($user);
            $app['session']->getFlashBag()->add("success", "Données mises à jour");
            return $app->redirect('/admin/users');
        } else {
            $app['session']->getFlashBag()->add("danger", "Erreur de formulaire, veuillez contacter votre Admin");
        }
        return false;
    }
    
    public function adminCreateUser(Request $request, Application $app) {
        $error = false;
        $user = new UserModel();
        
        if ($request->isMethod('POST')) {
            // test des valeurs entrées
            $user_email = \trim($request->request->get('user_email'));
            $user_firstname = \trim($request->request->get('user_firstname'));
            $user_lastname = \trim($request->request->get('user_lastname'));
            $role_id = \trim($request->request->get('role_id'));
            $user_status = \trim($request->request->get('user_status'));

            if (empty($user_email)) {
                $app['session']->getFlashBag()->add("danger", "Veuillez rentrer une adresse email");
                $error = true;
            }
            if (!empty($user_email) && !filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
                $app['session']->getFlashBag()->add("danger", "l'addresse email n'est pas valide");
                $error = true;
            }
            if (empty($user_firstname)) {
                $app['session']->getFlashBag()->add("danger", "Veuillez renseigner le prénom");
                $error = true;
            }
            if (empty($user_lastname)) {
                $app['session']->getFlashBag()->add("danger", "Veuillez renseigner le nom");
                $error = true;
            }
            if (empty($role_id)) {
                $app['session']->getFlashBag()->add("danger", "L'utilisateur doit posséder un rôle");
                $error = true;
            }
            if (empty($user_status)) {
                $app['session']->getFlashBag()->add("danger", "L'utilisateur doit posséder un statut");
                $error = true;
            }
            
            if($error) {
                return $app->redirect('/admin/users');
            } else {
                // je créée le tableau des valeurs à enregistrer
                $user->setFirstname($user_firstname);
                $user->setLastname($user_lastname);
                $user->setEmail($user_email);
                $user->setStatus($user_status);
                $user->setRole($role_id);
                
                
                var_dump($user);
                // j'enregistre les données en base
                $app['dao.user']->adminSaveUser($user);
                $app['session']->getFlashBag()->add("success", "Données mises à jour");
                return $app->redirect('/admin/users');
            }
            
            
            
        }
    }

}
        