<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

//use Symfony\Component\HttpFoundation\RedirectResponse;
//use InvalidArgumentException;
//use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

/**
 * Description of profileController
 *
 * @author Valneo
 */
class AccessController {

    /**
     * 
     * @param Application $app
     * @return type
     */
    public function loginAction(Application $app) {

        return $app['twig']->render('login.html.twig');
    }

    /**
     * 
     * @param Request $request
     * @param Application $app
     * @return type
     */
    public function loginUserAction(Request $request, Application $app) {
        $error = false;
        
        if ($request->isMethod('POST')) {            
            $email = \trim($request->request->get('user_email'));
            $plainPassword = \trim($request->request->get('user_password'));

            $user = $app['dao.user']->findEmail($email);
            //var_dump($user->getStatus());

            if (empty($email)) {
                $app['session']->getFlashBag()->add("danger", "Veuillez rentrer une adresse email");
                $error = true;
            } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $app['session']->getFlashBag()->add("danger", "l'addresse email n'est pas valide");
                $error = true;
            } elseif (filter_var($email, FILTER_VALIDATE_EMAIL) && !$user) {
                $app['session']->getFlashBag()->add("danger", "l'addresse email n'existe pas en base.\n Veuillez retaper votre adresse email ou contacter votre manager.");
                $error = true;
            } elseif (filter_var($email, FILTER_VALIDATE_EMAIL) && ($user->getStatus() == NULL || $user->getStatus() == 0)) {

                $app['session']->getFlashBag()->add("danger", "Vous devez vous enregistrer");
                $error = true;
            } else {
                $error = false;
            }

            if (empty($plainPassword)) {
                $app['session']->getFlashBag()->add("danger", "le mot de passe doit être renseigné");
                $error = true;
            }

            if (!$error) {
                echo 'je suis passe';
                $password = $user->getPassword();
                $salt = $user->getSalt();
                // get the encoding type
                $encoder = $app['security.encoder.bcrypt'];
                // Match the password
                $bool = ($encoder->isPasswordValid($password, $plainPassword, $salt));
                //echo 'bool ' . $bool;                
                if ($bool) {
                    // je logue et j'enregistre en session le user                    

                    $app['session']->set('user', $user);
//                    
                    return $app->redirect('/dashboard');
                } else {
                    $app['session']->getFlashBag()->add("danger", "les mots de passe ne sont pas exacts");

                    return $app['twig']->render('login.html.twig');
                }
            }
            return $app['twig']->render('login.html.twig');
        }
    }

    /**
     * 
     * @param Application $app
     * @return type
     */
    public function registerAction(Application $app) {

        return $app['twig']->render('register.html.twig');
    }

    /**
     * 
     * @param Request $request
     * @param Application $app
     * @return type
     */
    public function registerUserAction(Request $request, Application $app) {

        if ($request->isMethod('POST')) {
            $error = false;
            // récupération de l'email et du mote de passe en base de donnée
            $email = trim($request->request->get('email'));
            $plainPassword = trim($request->request->get('password'));
            $confirm_password = $request->request->get('confirm_password');
            // test de validité des infos passées par le user
            $user = $app['dao.user']->findEmail($email);

            // test de l'email
            if (empty($email)) {
                $app['session']->getFlashBag()->add("danger", "Veuillez rentrer une adresse email");
                $error = true;
            } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $app['session']->getFlashBag()->add("danger", "l'addresse email n'est pas valide");
                $error = true;
            } elseif (filter_var($email, FILTER_VALIDATE_EMAIL) && !$user) {
                $app['session']->getFlashBag()->add("danger", "l'addresse email n'existe pas en base.\n Veuillez retaper votre adresse email ou contacter votre manager.");
                $error = true;
            } elseif (filter_var($email, FILTER_VALIDATE_EMAIL) && $user->getStatus() == 1) {
                $app['session']->getFlashBag()->add("danger", "Vous êtes déjà enregistré");
                $error = true;
            } elseif ($user->getPassword() !== NULL) {
                $app['session']->getFlashBag()->add("danger", "Vous êtes déjà enregistré");
                $error = true;
            } else {
                // test du password
                if (empty($plainPassword)) {
                    $app['session']->getFlashBag()->add("danger", "le mot de passe doit être renseigné");
                    $error = true;
                } elseif (strlen($plainPassword) < 4) {
                    $app['session']->getFlashBag()->add("danger", "le mot de passe doit comporter au moins 5 caractères");
                    $error = true;
                } elseif ($plainPassword != $confirm_password) {
                    $app['session']->getFlashBag()->add("danger", "les deux passwords ne correpondent pas");
                    $error = true;
                } else {
                    $error = false;
                }
            }

            if ($error) {

                return $app['twig']->render('register.html.twig');
            } else {
                //echo 'toto';
                // email existe et update user
                // je recherche les données déja en base
                $user = $app['dao.user']->findEmail($email);

                $salt = substr(md5(time()), 0, 23);
                $user->setSalt($salt);

                // encodage de password                
                $user->setPlainPassword($plainPassword);

                // find the default encoder
                $encoder = $app['security.encoder.bcrypt'];
                $password = $encoder->encodePassword($plainPassword, $user->getSalt());

                $user->setPassword($password);

                $status = 1; //actif
                $user->setStatus($status);

                $app['dao.user']->saveUser($user);
                $app['session']->set('user', $user);
                $app['session']->getFlashBag()->add("success", "Activation de votre compte est réussie.");

                return $app->redirect('/dashboard');
                //return $app['twig']->render('.html.twig');
            }
        }

        return $app['twig']->render('register.html.twig');
    }

    /**
     * 
     * @param Request $request
     * @param Application $app
     * @return type
     */
    public function logoutUserAction(Request $request, Application $app) {
        
        $session = $request->getSession();
        $session_id = $session->getId();
        echo $session_id;
        $session->remove('user_id');
        $session->invalidate();
        die();

        return $app['twig']->render('login.html.twig');
    }
    
    /**
     * 
     * @param Request $request
     * @param Application $app
     * @return type
     */
    public function page404Action(Request $request, Application $app) {
        
        return $app['twig']->render('pages/page404.html.twig');
    }

}
