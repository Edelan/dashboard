<?php

/*
 * The MIT License
 *
 * Copyright 2018 edelan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Controller;

use Silex\Application;


/**
 * Description of RhController
 *
 * @author edelan
 */
class RhController {
    public function rhUsersAction(Application $app) {
        $users = $app['dao.user']->findAll();
        $services = $app['dao.userSettings']->findServices();
        $managers = $app['dao.userSettings']->findManagers();
        $roles = $app['dao.userSettings']->findRoles();
        $jobs = $app['dao.userSettings']->findJobs();
        $contracts = $app['dao.userSettings']->findContracts();
        
        $settings = ['services' => $services, 'managers' => $managers, 'roles' => $roles, 'jobs' => $jobs, 'contracts' => $contracts];
        
    return $app['twig']->render('manager/manager-rh-users.html.twig', [ 'users' => $users, 'settings' => $settings, 'user' => $app['session']->get('user') ]);
    }
    
    
}
