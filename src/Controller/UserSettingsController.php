<?php

/*
 * The MIT License
 *
 * Copyright 2018 edelan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Controller;

use App\Model\UserSettingsModel;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of UserSettingsController
 *
 * @author edelan
 */
class UserSettingsController {
    //put your code here
    public function settingsServiceAction(Application $app) {
        $services = $app['dao.userSettings']->findServices();
        
        return $app['twig']->render('settings/settings-services.html.twig', ['services' => $services]);
    }
    
    public function settingsServiceCreate( Request $request, Application $app) {
        $service = new UserSettingsModel();
        
        if ($request->isMethod('POST')) {
            $serv_name = \trim($request->request->get('serv_name'));            
            
            $service->setServ_name($serv_name);
            $app['dao.userSettings']->saveService($service);
            
            
            
            return $app->redirect('/rh/service');;
            
        }
    }
    
    public function settingsServiceUpdate(Request $request, Application $app) {
        
        $service = new UserSettingsModel();        
        
        if ($request->isMethod('POST')) {
            
            $serv_id = \trim($request->request->get('serv_id'));
            $serv_name = \trim($request->request->get('serv_name'));           
            
            $service->setServ_id($serv_id);
            $service->setServ_name($serv_name);
            $app['dao.userSettings']->saveService($service);
            
            return $app->redirect('/rh/service');
            
        }
    }
    
    public function settingsServiceDelete(Request $request, Application $app) {
        $id = \trim($request->request->get('serv_id'));
            $app['dao.userSettings']->deleteService($id);           
            
                                
            return $app->redirect('/rh/service');
    }
    
    public function settingsJobAction(Application $app) {
        $jobs = $app['dao.userSettings']->findJobs();
        
        return $app['twig']->render('settings/settings-jobs.html.twig', ['jobs' => $jobs]);
    }
    
    public function settingsJobCreate( Request $request, Application $app) {
        $job = new UserSettingsModel();
        
        if ($request->isMethod('POST')) {
            $job_name = \trim($request->request->get('job_name'));            
            
            $job->setJob_name($job_name);
            $app['dao.userSettings']->saveJob($job);
            
            
            
            return $app->redirect('/rh/job');
            
        }
    }
    
    public function settingsJobUpdate(Request $request, Application $app) {
        
        $job = new UserSettingsModel();        
        
        if ($request->isMethod('POST')) {
            
            $job_id = \trim($request->request->get('job_id'));
            $job_name = \trim($request->request->get('job_name'));           
            
            $job->setJob_id($job_id);
            $job->setJob_name($job_name);
            $app['dao.userSettings']->saveJob($job);
            
            return $app->redirect('/rh/job');
            
        }
    }
    
    public function settingsJobDelete(Request $request, Application $app) {
        $id = \trim($request->request->get('job_id'));
            $app['dao.userSettings']->deleteJob($id);           
            
                                
            return $app->redirect('/rh/job');
    }
    
    public function settingsContractAction(Application $app) {
        $contracts = $app['dao.userSettings']->findContracts();
        
        return $app['twig']->render('settings/settings-contracts.html.twig', ['contracts' => $contracts]);
    }
    
    public function settingsContractCreate( Request $request, Application $app) {
        $contract = new UserSettingsModel();
        
        if ($request->isMethod('POST')) {
            $contract_name = \trim($request->request->get('contract_name'));            
            
            $contract->setContract_name($contract_name);
            $app['dao.userSettings']->saveContract($contract);
            
            return $app->redirect('/rh/contract');
            
        }
    }
    
    public function settingsContractUpdate(Request $request, Application $app) {
        
        $contract = new UserSettingsModel();        
        
        if ($request->isMethod('POST')) {
            
            $contract_id = \trim($request->request->get('contract_id'));
            $contract_name = \trim($request->request->get('contract_name'));           
            
            $contract->setContract_id($contract_id);
            $contract->setContract_name($contract_name);
            $app['dao.userSettings']->saveContract($contract);
            
            return $app->redirect('/rh/contract');
            
        }
    }
    
    public function settingsContractDelete(Request $request, Application $app) {
        $id = \trim($request->request->get('contract_id'));
            $app['dao.userSettings']->deleteContract($id);           
            
                                
            return $app->redirect('/rh/contract');
    }
}
