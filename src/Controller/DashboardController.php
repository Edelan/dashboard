<?php

/*
 * The MIT License
 *
 * Copyright 2018 Your Name <e.delanoy@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Controller;

use Silex\Application;

/**
 * Description of dashboardController
 *
 * @author Your Name <e.delanoy@gmail.com>
 */
class DashboardController {

    public function dashboardAction(Application $app) {

        //echo $id;
        $user = $app['session']->get('user');
        if (is_null($user)) {
            return $app->redirect('/page404');
        } else {
            $user_id = $user->getId();

            // Récupération de données de calendrier
            $data = $app['dao.calendar']->findByIdJson($user_id);
            $cal = \json_encode($data);

            // Récupération des données de todolist
            $todolist = $app['dao.todolist']->findAllByIdJson($user_id);

            return $app['twig']->render('pages/dashboard.html.twig', ['user' => $user, 'calendrier' => $cal, 'todolist' => $todolist]);
        }
    }

}
