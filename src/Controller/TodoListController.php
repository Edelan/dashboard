<?php

/*
 * The MIT License
 *
 * Copyright 2018 Your Name <e.delanoy@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Controller;

use Silex\Application;
use App\Model\TodoListModel;
use Symfony\Component\HttpFoundation\Request;
/**
 * Description of TodoListController
 *
 * @author Your Name <e.delanoy@gmail.com>
 */
class TodoListController {
    
    public function TodoListAction(Application $app) {
        //echo $id;
        $user = $app['session']->get('user');
        $user_id = $user->getId();
        //var_dump($user);
        //echo $user_id;
        $todolist = $app['dao.todolist']->findAllByIdJson($user_id);
        //var_dump($todolist);
        //var_dump(json_encode($todolist));
        //$todolist = \json_encode($todolist);
        if (!empty($todolist)) {
            return $app['twig']->render('pages/todo-list.html.twig', ['todolist' => $todolist]);
            } else {
            $todolist=[];    
            return $app['twig']->render('pages/todo-list.html.twig', ['todolist' => $todolist]);
            }
    }
    
    public function TodoListAddEvent(Request $request, Application $app) {
        $error = false;
        $todo = new TodoListModel();
         if ($request->isMethod('POST')) {
             //var_dump($_POST);
             $content = \trim($request->request->get('todo_content'));             
             $user_id = \trim($request->request->get('user_id'));
             
             if(empty($content)){
                $app['session']->getFlashBag()->add("danger", "Pas de champ vide");
                $error = true; 
             }
             
             if($error) {
                return $app->redirect('/todolist');
            } else {
                // je créée le tableau des valeurs à enregistrer
                $todo->setContent($content);                
                $todo->setUser_id($user_id);
                
                // j'enregistre les données en base
                $app['dao.todolist']->saveTodo($todo);
                $app['session']->getFlashBag()->add("success", "Données enregistrées");
                
                                               
                return $app->redirect('/todolist');
            }
         }
    }
    public function TodoListUpdateEvent(Request $request, Application $app) {
        $error = false;
        $todo = new TodoListModel();
        if($request->isMethod('POST')) {
            $id = \trim($request->request->get('todo_id'));
            $content = \trim($request->request->get('todo_content'));             
            $user_id = \trim($request->request->get('user_id'));
            
            if(empty($content)){
                $app['session']->getFlashBag()->add("danger", "Pas de champ vide");
                $error = true; 
             }
             if($error) {
                return $app->redirect('/todolist');
            } else {
                // je créée le tableau des valeurs à enregistrer
                $todo->setId($id);
                $todo->setContent($content);
                $todo->setUser_id($user_id);
                
                // j'enregistre les données en base
                $app['dao.calendar']->saveTodo($todo);
                $app['session']->getFlashBag()->add("success", "Données enregistrées");
                                
                return $app->redirect('/todolist');
            }
        }
    }
    
    public function TodoListDeleteEvent(Request $request, Application $app) {
         if ($request->isMethod('POST')) {
            $id = \trim($request->request->get('todo_id'));
            $app['dao.todolist']->deleteTodo($id);
            
            $app['session']->getFlashBag()->add("success", "Données supprimées");
                                
                return $app->redirect('/todolist');
                    
            
         }
    }
}
