<?php

/*
 * The MIT License
 *
 * Copyright 2018 edelan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Controller;

use App\Model\CalendarModel;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of CalendarController
 *
 * @author edelan
 */
class CalendarController {
    public function CalendarAction(Application $app) {
        $user = $app['session']->get('user');
        $user_id = $user->getId();
        //var_dump();
        //die();
        $result = $app['dao.calendar']->findByIdJson($user_id);
                
        //var_dump(json_encode($cal));
        $cal = \json_encode($result);
        if (!empty($cal)) {
            return $app['twig']->render('pages/calendar.html.twig', ['calendar' => $cal]);
            } else {
                $cal = [];
                return $app['twig']->render('pages/calendar.html.twig', ['calendar' => $cal]);
            }
        }
        
   
    
    public function CalendarAddEvent(Request $request, Application $app) {
        $error = false;
        $cal = new CalendarModel();
         if ($request->isMethod('POST')) {
             //var_dump($_POST);
             $title = \trim($request->request->get('cal_title'));
             $start = \trim($request->request->get('cal_start'));
             $end = \trim($request->request->get('cal_end'));
             $descrip = \trim($request->request->get('cal_descrip'));
             $user_id = \trim($request->request->get('user_id'));
             
             if(empty($title)){
                $app['session']->getFlashBag()->add("danger", "un titre est obligatoire");
                $error = true; 
             }
             
             if($error) {
                return $app->redirect('/calendar');
            } else {
                // je créée le tableau des valeurs à enregistrer
                $cal->setTitle($title);
                $cal->setStart($start);
                $cal->setEnd($end);
                $cal->setDescrip($descrip);
                $cal->setUser_id($user_id);
                
                // j'enregistre les données en base
                $app['dao.calendar']->saveEventCal($cal);
                $app['session']->getFlashBag()->add("success", "Données enregistrées");
                                
                return $app->redirect('/calendar');
            }
         }
    }
    public function CalendarUpdateEvent(Request $request, Application $app) {
        $error = false;
        $cal = new CalendarModel();
        if($request->isMethod('POST')) {
            $id = \trim($request->request->get('cal_id'));
            $title = \trim($request->request->get('cal_title'));
            $start = \trim($request->request->get('cal_start'));
            $end = \trim($request->request->get('cal_end'));
            $descrip = \trim($request->request->get('cal_descrip'));
            $user_id = \trim($request->request->get('user_id'));
            
            if(empty($title)){
                $app['session']->getFlashBag()->add("danger", "un titre est obligatoire");
                $error = true; 
             }
             if($error) {
                return $app->redirect('/calendar');
            } else {
                // je créée le tableau des valeurs à enregistrer
                $cal->setId($id);
                $cal->setTitle($title);
                $cal->setStart($start);
                $cal->setEnd($end);
                $cal->setDescrip($descrip);
                $cal->setUser_id($user_id);
                
                // j'enregistre les données en base
                $app['dao.calendar']->saveEventCal($cal);
                $app['session']->getFlashBag()->add("success", "Données enregistrées");
                                
                return $app->redirect('/calendar');
            }
        }
    }
    
    public function CalendarDeleteEvent(Request $request, Application $app) {
         if ($request->isMethod('POST')) {
            $id = \trim($request->request->get('cal_id'));
            $app['dao.calendar']->deleteEventCal($id);
            
            $app['session']->getFlashBag()->add("success", "Données supprimées");
                                
                return $app->redirect('/calendar');
                    
            
         }
    }
    
    
}
