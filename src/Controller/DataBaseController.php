<?php

/*
 * The MIT License
 *
 * Copyright 2018 edelan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Controller;

use Silex\Application;

/**
 * Description of DataBaseController
 *
 * @author edelan
 */
class DataBaseController {
    //put your code here
    function sizeDataBase(Application $app) {
        $database = $app['dao.database']->getSizeDataBase();        
        
        return $app['twig']->render('admin/admin-database.html.twig', [ 'database' => $database]);
    }
    
    function backupDataBase(Application $app) {
        $dbname[0] = $app['db.options']['dbname'];
        $user[0] = $app['db.options']['user'];
        $password[0] = $app['db.options']['password'];
        $host[0] = $app['db.options']['host'];


        $nb_loop = count($dbname) - 1;
        for ($i = 0; $i <= $nb_loop; $i++) {
            //var_dump(dirname(__DIR__ ,2));
            $export_path =dirname(__DIR__ ,2). DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . $dbname[$i] . '_' . date("Y-m-d-H-i-s") . '.zip';
            //var_dump($export_path);
            
            $command = 'mysqldump --opt -h ' . $host[$i] . ' -u ' . $user[$i] . ' -p' . $password[$i] . ' ' . $dbname[$i] . ' > ' . $export_path;
            $output = array();            
            exec($command, $output, $status);
            
            

            switch ($status) {
                case 0:
                    echo 'Base de données <b>' . $dbname[$i] . '</b> exporté avec succès vers l\'emplacement <b>' . $export_path . '</b><br/>';
                    break;
                case 1:
                    echo 'Il y a eu un message d\avertissement durant l\'export de la base <b>' . $dbname[$i] . '</b> vers <b>' . $export_path . '</b><br/>';
                    break;
                case 2:
                    echo 'Il y a eu un message d\'erreur durant l\'export. Veuillez vérifier vos valeurs : <br/>
                 <br/>
                 <table>
                    <tr>
                        <td>MySQL Database Name:</td>
                        <td><b>' . $dbname[$i] . '</b></td>
                    </tr>
                    <tr>
                        <td>MySQL User Name:</td>
                        <td><b>' . $user[$i] . '</b></td>
                    </tr>
                    <tr>
                        <td>MySQL Password:</td>
                        <td><b>NOTSHOWN</b></td>
                    </tr>
                    <tr>
                        <td>MySQL Host Name:</td>
                        <td><b>' . $host[$i] . '</b></td>
                    </tr>
                </table>
                <br/>';
                    break;
            }
            return $app->redirect('/admin/database');
        }
        
    }

}
