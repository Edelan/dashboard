<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Model\UserModel;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of UserController
 *
 * @author Valneo
 */
class UserController {

    /**
     * récupération de tous les users
     * @param Application $app
     * @return object Twig
     */
    public function UserGetAll(Application $app) {
        $users = $app['dao.user']->findAll();
        return $app['twig']->render('pages/user.html.twig', ['users' => $users]);
    }

    /**
     * récupération d'un utilisateur
     * @param int $id
     * @param Application $app
     * @return objet Twig
     */
    public function UserGetId(int $id, Application $app) {

        $user = $app['dao.user']->find($id);
        
        
        return $app['twig']->render('pages/user.html.twig', ['user' => $user]);
    }
    
    public function UserDisplayGetId(int $id, Application $app) {
        
        $userData = $app['dao.user']->find($id);
        $services = $app['dao.userSettings']->findServices();
        $managers = $app['dao.userSettings']->findManagers();
        $roles = $app['dao.userSettings']->findRoles();
        $jobs = $app['dao.userSettings']->findJobs();
        $contracts = $app['dao.userSettings']->findContracts();
        
        $settings = ['services' => $services, 'managers' => $managers, 'roles' => $roles, 'jobs' => $jobs, 'contracts' => $contracts];
        
    return $app['twig']->render('manager/manager-rh-user-display.html.twig', [ 'userData' => $userData, 'settings' => $settings, 'user' => $app['session']->get('user') ]);
        
        
    }
    
    public function UserDisplayById(Request $request, Application $app) {
        
        if ($request->isMethod('POST')) {
            $reqId = \trim($request->get('user_id'));           
          
            $result = $app['dao.user']->findArrayById($reqId);        
        
        return json_encode($result);
        }
    }

    // création d'un utilisateur par le formulaire sur profile.html.twig
    public function UserCreate(Request $request, Application $app) {
        
        $user = new UserModel();
        
        if ($request->isMethod('POST')) {
            // test des valeurs entrées
            $user_civility = \trim($request->request->get('user_civility'));
            $user_firstname = \trim($request->request->get('user_firstname'));
            $user_lastname = \trim($request->request->get('user_lastname'));
            $user_birthdate = \trim($request->request->get('user_birthdate'));
            $user_email_perso = \trim($request->request->get('user_email_perso'));
            $user_phone_perso = \trim($request->request->get('user_phone_perso'));
            $user_adress = \trim($request->request->get('user_adress'));
            $user_postal = \trim($request->request->get('user_postal'));
            $user_city = \trim($request->request->get('user_city'));
            $user_enter_date = \trim($request->request->get('user_enter_date'));
            $user_status = \trim($request->request->get('user_status'));
            $contract_id = \trim($request->request->get('contract_id'));
            $job_id = \trim($request->request->get('job_id'));
            $serv_id = \trim($request->request->get('serv_id'));
            $manager_id = \trim($request->request->get('manager_id'));
            $role_id = \trim($request->request->get('role_id'));
            
            $user->setCivility($user_civility);
            $user->setFirstname($user_firstname);
            $user->setLastname($user_lastname);
            $user->setBirthdate($user_birthdate);            
            $user->setEmail_perso($user_email_perso);
            $user->setPhone_perso($user_phone_perso);
            $user->setAdress($user_adress);
            $user->setPostal($user_postal);
            $user->setCity($user_city);
            $user->setEnter_date($user_enter_date);
            $user->setStatus($user_status);
            $user->setContract($contract_id);
            $user->setJob($job_id);
            $user->setService($serv_id);
            $user->setManager($manager_id);            
            $user->setRole($role_id);
                
                
                
                // j'enregistre les données en base
                $app['dao.user']->SaveUser($user);
                $app['session']->getFlashBag()->add("success", "Données mises à jour");
                return $app->redirect('/rh/users');
        }
    }

    
    public function UserUpdate($id, Request $request, Application $app) {
        //
        $user = $app['dao.user']->find($id);

        if ($request->isMethod('POST')) {
            
            
            // vérification des données simplement sur les inclusions
            $user_civility = \trim($request->request->get('user_civility'));
            $user_firstname = \trim($request->request->get('user_firstname'));
            $user_lastname = \trim($request->request->get('user_lastname'));
            $user_birthdate = \trim($request->request->get('user_birthdate'));
            $user_email_perso = \trim($request->request->get('user_email_perso'));
            $user_phone_perso = \trim($request->request->get('user_phone_perso'));
            $user_adress = \trim($request->request->get('user_adress'));
            $user_postal = \trim($request->request->get('user_postal'));
            $user_city = \trim($request->request->get('user_city'));
            $user_enter_date = \trim($request->request->get('user_enter_date'));
            $user_status = \trim($request->request->get('user_status'));
            $contract_id = \trim($request->request->get('contract_id'));
            $job_id = \trim($request->request->get('job_id'));
            $serv_id = \trim($request->request->get('serv_id'));
            $manager_id = \trim($request->request->get('manager_id'));
            $role_id = \trim($request->request->get('role_id'));
            
            $user->setCivility($user_civility);
            $user->setFirstname($user_firstname);
            $user->setLastname($user_lastname);
            $user->setBirthdate($user_birthdate);            
            $user->setEmail_perso($user_email_perso);
            $user->setPhone_perso($user_phone_perso);
            $user->setAdress($user_adress);
            $user->setPostal($user_postal);
            $user->setCity($user_city);
            $user->setEnter_date($user_enter_date);
            $user->setStatus($user_status);
            $user->setContract($contract_id);
            $user->setJob($job_id);
            $user->setService($serv_id);
            $user->setManager($manager_id);            
            $user->setRole($role_id);
            //$user->setId($request->request->get('user_id'));
//            $user->setCivility($request->request->get('user_civility'));
//            $user->setFirstname($request->request->get('user_firstname'));
//            $user->setLastname($request->request->get('user_lastname'));
//            $user->setBirthdate($request->request->get('user_birthdate'));
//            //$user->setPhoto($request->request-get('user_photo'));
//            $user->setEmail_perso($request->request->get('user_email_perso'));
//            $user->setPhone_perso($request->request->get('user_phone_perso'));
//            $user->setAdress($request->request->get('user_adress'));
//            $user->setCity($request->request->get('user_city'));            
//            $user->setPostal($request->request->get('user_postal'));
//            $user->setEnter_date($request->request->get('user_enter_date'));
//            $user->setContract($request->request->get('contract_id'));
//            $user->setJob($request->request->get('job_id'));
//            $user->setService($request->request->get('serv_id'));
//            $user->setManager($request->request->get('manager_id'));
//            $user->setRole($request->request->get('role_id'));
//            $user->setStatus($request->request->get('user_status'));
            //var_dump($user);
            $app['dao.user']->saveUser($user);
            
            $app['session']->getFlashBag()->add("success", "Données mises à jour");
            return $app->redirect('/rh/users');
        } else {
            $app['session']->getFlashBag()->add("danger", "Erreur de formulaire");
        }
        
    }
    
    

    // suppression d'un utilisateur (changement du status en inactif) par le formulaire sur profile.html.twig
    public function UserDelete(int $id, Application $app) {
        
    }
    
    public function rhUsersAction(Application $app) {
        $users = $app['dao.user']->findAll();
        $services = $app['dao.userSettings']->findServices();
        $managers = $app['dao.userSettings']->findManagers();
        $roles = $app['dao.userSettings']->findRoles();
        $jobs = $app['dao.userSettings']->findJobs();
        $contracts = $app['dao.userSettings']->findContracts();
        
        $settings = ['services' => $services, 'managers' => $managers, 'roles' => $roles, 'jobs' => $jobs, 'contracts' => $contracts];
        
    return $app['twig']->render('manager/manager-rh-users.html.twig', [ 'users' => $users, 'settings' => $settings, 'user' => $app['session']->get('user') ]);
    }

}
