<?php

/*
 * The MIT License
 *
 * Copyright 2018 Your Name <e.delanoy@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\DAO;

use App\Model\TodoListModel;

/**
 * Description of TodoListDao
 *
 * @author Your Name <e.delanoy@gmail.com>
 */
class TodoListDao extends DAO {

    //put your code here

    public function findAll(int $id) {        
        $sql = 'SELECT * '
                . 'FROM todolist AS t '
                . 'WHERE user_id= ?';

        $row = $this->getDb()->fetchAssoc($sql, array($id));
        var_dump($row);
        if ($row) {
            return $this->buildDomainObjet($row);
        } else {
            return false;
        }
    }
    
    public function findAllByIdJson(int $id) {
       $sql = 'SELECT * '
                . 'FROM todolist AS t '
                . 'WHERE user_id= ?';
        $row = $this->getDb()->fetchAll($sql, array($id));
        
        if ($row) {
            return ($row);
        } else {
            return false;
        }
    }

    public function saveTodo(TodoListModel $todo) {
        $todoData = [
            'todo_content' => $todo->getContent(),            
            'user_id' => $todo->getUser_id()
        ];
        var_dump($todoData);
        
        if($todo->getId()){
            // un évènement est déjà enregistré donc MAJ
            //echo 'id existant dans la base de donnees';
            
            
            $this->getDb()->update('todolist', $todoData, array('todo_id' => $todo->getId()));
        } else {
            
            echo 'id non existant';
            // l'évènement est nouveau donc enregistrement
            $this->getDb()->insert('todolist', $todoData);
            // récupération du dernier id et ajout d'une ligne
            $id = $this->getDb()->lastInsertId();
            $todo->setId($id);
        }
    }

    public function updateTodo($param) {
        
    }

    public function reorderTodo($param) {
        
    }

    public function deleteTodo(int $id) {
        $this->getDb()->delete('todolist', array('todo_id' => $id));
    }

    protected function buildDomainObjet(array $row) {
        $todo = new TodoListModel();

        $todo->setId($row['todo_id']);
        $todo->setContent($row['todo_content']);
        $todo->setDue($row['todo_due']);
        $todo->setDone($row['todo_done']);
        $todo->setOrder($row['todo_order']);
        $todo->setUser_id($row['todo_user_id']);

        return $todo;
    }

}
