<?php

/*
 * The MIT License
 *
 * Copyright 2018 edelan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\DAO;

use App\Model\UserSettingsModel;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of UserSettingsDao
 *
 * @author edelan
 */
class UserSettingsDao extends DAO {

//put your code here
    public function findServices() {
        $sql = 'SELECT * FROM service '
                . 'ORDER BY serv_name ASC ';

        $row = $this->getDb()->fetchAll($sql);

        if ($row) {

            $service = $row;
            return $service;
        } else {

            return false;
        }
    }

    public function findServiceById(int $id) {
        $sql = 'SELECT * FROM service '
                . 'WHERE serv_id =?';

        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row) {

            return $this->buildDomainObjetService($row);
        } else {

            return false;
        }
    }

    public function saveService(UserSettingsModel $service) {
        $data = [
            'serv_name' => $service->getServ_name()
        ];

        if ($service->getServ_id()) {
            // Update
            $this->getDb()->update('service', $data, array('serv_id' => $service->getServ_id()));
        } else {
            $this->getDb()->insert('service', $data);
            $id = $this->getDb()->lastInsertId();
            $service->setServ_id($id);
        }
    }
    
    public function deleteService($id) {
        $this->getDb()->delete('service', array('serv_id' => $id));
    }

    public function findManagers() {
        $sql = 'SELECT * FROM user AS u '
                . 'LEFT JOIN role AS r '
                . 'ON u.role_id = r.role_id '
                . 'WHERE r.role_name = "ROLE_MANAGER" '
                . 'ORDER BY u.user_lastname ASC';

        $row = $this->getDb()->fetchAll($sql);

        if ($row) {

            $manager = $row;
            return $manager;
        } else {

            return false;
        }
    }

    public function findRoles() {
        $sql = 'SELECT * FROM role '
                . 'ORDER BY role_name ASC ';

        $row = $this->getDb()->fetchAll($sql);

        if ($row) {

            $role = $row;
            return $role;
        } else {

            return false;
        }
    }

    public function findJobs() {
        $sql = 'SELECT * FROM job '
                . 'ORDER BY job_name ASC ';

        $row = $this->getDb()->fetchAll($sql);

        if ($row) {

            $job = $row;
            return $job;
        } else {

            return false;
        }
    }
    
    public function findJobById(int $id) {
        $sql = 'SELECT * FROM job '
                . 'WHERE job_id =?';

        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row) {

            return $this->buildDomainObjetJob($row);
        } else {

            return false;
        }
    }

    public function saveJob(UserSettingsModel $job) {
        $data = [
            'job_name' => $job->getJob_name()
        ];

        if ($job->getJob_id()) {
            // Update
            $this->getDb()->update('job', $data, array('job_id' => $job->getJob_id()));
        } else {
            $this->getDb()->insert('job', $data);
            $id = $this->getDb()->lastInsertId();
            $job->setJob_id($id);
        }
    }
    
    public function deleteJob($id) {
        $this->getDb()->delete('job', array('job_id' => $id));
    }

    public function findContracts() {
        $sql = 'SELECT * FROM contract '
                . 'ORDER BY contract_name ASC ';

        $row = $this->getDb()->fetchAll($sql);

        if ($row) {

            $contract = $row;
            return $contract;
        } else {

            return false;
        }
    }
    
     public function findContractById(int $id) {
        $sql = 'SELECT * FROM contract '
                . 'WHERE contract_id =?';

        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row) {

            return $this->buildDomainObjetContract($row);
        } else {

            return false;
        }
    }

    public function saveContract(UserSettingsModel $contract) {
        $data = [
            'contract_name' => $contract->getContract_name()
        ];

        if ($contract->getContract_id()) {
            // Update
            $this->getDb()->update('contract', $data, array('contract_id' => $contract->getContract_id()));
        } else {
            $this->getDb()->insert('contract', $data);
            $id = $this->getDb()->lastInsertId();
            $contract->setContract_id($id);
        }
    }
    
    public function deleteContract($id) {
        $this->getDb()->delete('contract', array('contract_id' => $id));
    }

    protected function buildDomainObjet(array $row) {
        
    }

    protected function buildDomainObjetService(array $row) {
        $service = new UserSettingsModel();

        $service->setServ_id($row['serv_id']);
        $service->setServ_name($row['serv_name']);

        return $service;
    }

    protected function buildDomainObjetJob(array $row) {
        $job = new UserSettingsModel();

        $job->setJob_id($row['job_id']);
        $job->setJob_name($row['job_name']);

        return $job;
    }

    protected function buildDomainObjetContract(array $row) {
        $contract = new UserSettingsModel();

        $contract->setContract_id($row['contract_id']);
        $contract->setContract_name($row['contract_name']);

        return $contract;
    }

}
