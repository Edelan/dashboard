<?php

namespace App\DAO;

use App\Model\UserModel;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Description of UserDAO
 *
 * @author Valneo
 */
class UserDAO extends DAO implements UserProviderInterface {

    
    /**
     * find all user
     * @return array list ot users
     */
    public function findAll(): array {
        $sql = 'SELECT * '
                . 'FROM user AS u '
                . 'LEFT JOIN role AS r '
                . 'ON u.role_id = r.role_id '
                . 'LEFT JOIN service AS s '
                . 'ON u.serv_id = s.serv_id '
                . 'LEFT JOIN job AS j '
                . 'ON u.job_id = j.job_id '
                . 'LEFT JOIN contract As c '
                . 'ON u.contract_id = c.contract_id ';
        $rows = $this->getDb()->fetchAll($sql);

        // convert query result to an array
        $users = array();
        foreach ($rows as $row) {
            //var_dump($row);
            $userId = $row['user_id'];
            $users[$userId] = $this->buildDomainObjet($row);
        }
        return $users;
    }    

    /**
     * find user by id
     * @param int $id the user id
     * @return type
     */
    public function find( int $id) {
        $sql = 'SELECT * '
                . 'FROM user AS u '
                . 'LEFT JOIN role AS r '
                . 'ON u.role_id = r.role_id '
                . 'LEFT JOIN service AS s '
                . 'ON u.serv_id = s.serv_id '
                . 'LEFT JOIN job AS j '
                . 'ON u.job_id = j.job_id '
                . 'LEFT JOIN contract As c '
                . 'ON u.contract_id = c.contract_id '
                . 'WHERE user_id= ?';
        $row =$this->getDb()->fetchAssoc($sql, array($id));
        
        if ($row) {
            return $this->buildDomainObjet($row);
        } else {
            return false;
        }
    }
    public function findId(int $id) {
        $sql = 'SELECT * '
                . 'FROM user AS u '
                . 'LEFT JOIN role AS r '
                . 'ON u.role_id = r.role_id '                
                . 'LEFT JOIN service AS s '
                . 'ON u.serv_id = s.serv_id '
                . 'LEFT JOIN job AS j '
                . 'ON u.job_id = j.job_id '
                . 'LEFT JOIN contract As c '
                . 'ON u.contract_id = c.contract_id '
                . 'WHERE user_id= ?';
        $row = $this->getDb()->fetchAll($sql, array($id));
        
        if ($row) {
            return json_encode($row);
        } else {
            return false;
        }
    }
    
    public function findArrayById($id) {
        $sql = 'SELECT * '
                . 'FROM user AS u '
                . 'LEFT JOIN role AS r '
                . 'ON u.role_id = r.role_id '                
                . 'LEFT JOIN service AS s '
                . 'ON u.serv_id = s.serv_id '
                . 'LEFT JOIN job AS j '
                . 'ON u.job_id = j.job_id '
                . 'LEFT JOIN contract As c '
                . 'ON u.contract_id = c.contract_id '
                . 'WHERE user_id= ?';
        $row = $this->getDb()->fetchAssoc($sql, array($id));
        
        if ($row) {
            
            $user = $row;
            return $user;
        } else {
            return false;
        }
    }
    public function findArrayId($id) {
        $toto = 'tata';
            return $toto;
        }    

    /**
     * 
     * @param string $email
     * @return boolean
     */
    public function findEmail(string $email) {
        $email = trim($email);        
        $sql = 'SELECT * FROM user AS u '
                . 'LEFT JOIN role AS r '
                . 'ON u.role_id = r.role_id '                
                . 'LEFT JOIN service AS s '
                . 'ON u.serv_id = s.serv_id '
                . 'LEFT JOIN job AS j '
                . 'ON u.job_id = j.job_id '
                . 'LEFT JOIN contract As c '
                . 'ON u.contract_id = c.contract_id '
                . 'WHERE user_email= ?';
        $row = $this->getDb()->fetchAssoc($sql, array($email));
        
        if ($row) {            
            return $this->buildDomainObjet($row);
        } else {            
            return false;
        }
    }
    
    // create user ou update user on password
    public function saveUser(UserModel $user) {
        
        $userData = [
            'user_firstname' => $user->getFirstname(),
            'user_lastname' => $user->getLastname(),
            'user_birthdate' => $user->getBirthdate(),
            'user_civility' => $user->getCivility(),            
            'user_adress' => $user->getAdress(),
            'user_city' => $user->getCity(),
            'user_country' => $user->getCountry(),
            'user_postal' => $user->getPostal(),
            'user_phone' => $user->getPhone(),
            'user_phone_perso' => $user->getPhone_perso(),
            'user_email' => $user->getEmail(),
            'user_email_perso' => $user->getEmailPerso(),                        
            'user_password' => $user->getPassword(),
            'user_salt' => $user->getSalt(),
            'user_enter_date' => $user->getEnter_date(),
            'user_status' => $user->getStatus(),            
            'manager_id' => $user->getManager(),
            'role_id' => $user->getRole(),
            'serv_id' => $user->getService(),
            'job_id' => $user->getJob(),
            'contract_id' => $user->getContract(),
        ];
        
        if($user->getId()){
            // l'utilisateur est déjà enregistré donc MAJ
            //echo 'id existant dans la se de donnee';
            // je mets à jour la date_updated
            $userData['date_updated'] = date('Y-m-d H:i:s');
            $this->getDb()->update('user', $userData, array('user_id' => $user->getId()));
        } else {
            
            //echo 'id non existant';
            // utilisable par admin et manager
            // l'utilisateur est nouveau donc enregistrement
            // j'enregistre la date de création
            $userData['date_created'] = date('Y-m-d H:i:s');
            $this->getDb()->insert('user', $userData);
            // récupération de l'utilisateur créé et affectation
            $id = $this->getDb()->lastInsertId();
            $user->setId($id);
        }
    }
    // create user ou update user on password
    public function adminSaveUser(UserModel $user) {
        
        $userData = [
            'user_firstname' => $user->getFirstname(),
            'user_lastname' => $user->getLastname(),            
            'user_email' => $user->getEmail(),            
            'user_password' => $user->getPassword(),            
            'user_status' => $user->getStatus(),           
            'role_id' => $user->getRole()
        ];
        
        if($user->getId()){
            // l'utilisateur est déjà enregistré donc MAJ
            //echo 'id existant dans la se de donnee';
            // je mets à jour la date_updated
            $userData['date_updated'] = date('Y-m-d H:i:s');
            $this->getDb()->update('user', $userData, array('user_id' => $user->getId()));
        } else {
            
            echo 'id non existant';
            // utilisable par admin et manager
            // l'utilisateur est nouveau donc enregistrement
            // j'enregistre la date de création
            $userData['date_created'] = date('Y-m-d H:i:s');
            $this->getDb()->insert('user', $userData);
            // récupération de l'utilisateur créé et affectation
            $id = $this->getDb()->lastInsertId();
            $user->setId($id);
        }
    }
    
    // delete user
    public function deleteUser($id) {
        // change le status de user est gérer par la fonction update
        // voir la relation avec la table employé
    }

    protected function buildDomainObjet(array $row): UserModel {
        $user = new UserModel();
        $user->setId($row['user_id']);
        $user->setEmail($row['user_email']);
        $user->setPassword($row['user_password']);        
        $user->setSalt($row['user_salt']);
        $user->setFirstname($row['user_firstname']);
        $user->setLastname($row['user_lastname']);
        $user->setName($row['user_firstname'], $row['user_lastname']);
        $user->setBirthdate($row['user_birthdate']);
        $user->setPhoto($row['user_photo']);
        $user->setAdress($row['user_adress']);
        $user->setCity($row['user_city']);
        $user->setPostal($row['user_postal']);
        $user->setCountry($row['user_country']);
        $user->setPhone($row['user_phone']);
        $user->setPhone_perso($row['user_phone_perso']);
        $user->setEmailPerso($row['user_email_perso']);
        $user->setStatus($row['user_status']);
        $user->setEnter_date($row['user_enter_date']);
        // récupérer les valeurs dans les autres tables
        $user->setCivility($row['user_civility']);
        $user->setService($row['serv_id']);
        $user->setService_name($row['serv_name']);
        $user->setJob($row['job_id']);
        $user->setJob_name($row['job_name']);
        $user->setRole($row['role_id']);
        $user->setManager($row['manager_id']);
        $user->setRole_name($row['role_name']);
        //$user->setManager_name($row['manager_name']);
        $user->setContract($row['contract_id']);
        
                
        return $user;
        
    }
     
    // No used for this project its an experimental implementation
    public function loadUserByUsername($username): UserInterface {
        $sql = 'SELECT * FROM user WHERE user_email= ?';
        $row = $this->getDb()->fetchAssoc($sql, array($username));
        
        if($row) {
            return $this->buildDomainObjet($row);
        } else {            
            throw new UsernameNotFoundException(sprintf('User "%s" not found. Please see your Admin', $username));
        }
    }

    public function refreshUser(UserInterface $user): UserInterface {
        $class= get_class($user);
        if(!$this->supportsClass($class)){
            throw new UnsupportedUserException(sprintf('Instance of "%s3 are not supported.', $class));
        }
        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class): bool {
        return 'App\model\UserModel' === $class;
    }
}
