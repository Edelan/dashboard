<?php

namespace App\DAO;
use Doctrine\DBAL\Connection;

/**
 * Description of DAO
 *
 * @author Valneo
 */
abstract class DAO {
    private $db;
    
    /**
     * Connection à la base de données
     * @param Connection $db
     */
    public function __construct(Connection $db) {
        $this->db = $db;
    }
    
    /**
     * récupération de la bdd
     * @return type
     */
    public function getDb() {
        return $this->db;
    }
    
    /**
     * 
     */
    abstract protected function buildDomainObjet(array $row);
}
