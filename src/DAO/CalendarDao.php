<?php

/*
 * The MIT License
 *
 * Copyright 2018 edelan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\DAO;

use App\Model\CalendarModel;

/**
 * Description of CalendarDao
 *
 * @author edelan
 */
class CalendarDao extends Dao {

    //put your code here
    public function findById(int $id) {
        $sql = 'SELECT * FROM calendar AS c '
                . 'ORDER BY cal_id '
                . 'WHERE user_id= ?';

        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row) {
            return $this->buildDomainObjet($row);
        } else {
            return false;
        }
    }
    
    public function findByIdJson(int $id) {
        $sql = 'SELECT cal_id AS id, cal_title AS title, cal_start AS start, cal_end AS end, cal_descrip AS descrip FROM calendar '                
                . 'WHERE user_id= ?';
        $row = $this->getDb()->fetchAll($sql, array($id));
        
        if ($row) {
            return ($row);
        } else {
            return false;
        }
    }
    
    public function saveEventCal(CalendarModel $cal) {
        $calData = [
            'cal_title' => $cal->getTitle(),
            'cal_start' => $cal->getStart(),
            'cal_end' => $cal->getEnd(),
            'cal_descrip' => $cal->getDescrip(),
            'user_id' => $cal->getUser_id()
        ];        
        
        if($cal->getId()){
            // un évènement est déjà enregistré donc MAJ
            //echo 'id existant dans la base de donnees';
            
            
            $this->getDb()->update('calendar', $calData, array('cal_id' => $cal->getId()));
        } else {
            
            echo 'id non existant';
            // l'évènement est nouveau donc enregistrement
            $this->getDb()->insert('calendar', $calData);
            // récupération du dernier id et ajout d'une ligne
            $id = $this->getDb()->lastInsertId();
            $cal->setId($id);
        }
    }
    
    public function deleteEventCal(int $id) {
        // Delete the event
        $this->getDb()->delete('calendar', array('cal_id' => $id));        
        
    }

    protected function buildDomainObjet(array $row) {
        $cal = new CalendarModel();
        $cal->setId($row['cal_id']);
        $cal->setTitle($row['cal_title']);
        $cal->setStart($row['cal_start']);
        $cal->setEnd($row['cal_end']);
        $cal->setDescrip($row['cal_descrip']);
        $cal->setUser_id($row['user_id']);

        return $cal;
    }

}
