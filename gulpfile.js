let gulp = require('gulp');
let sass = require('gulp-sass');
let autoPrefixer = require('gulp-autoprefixer');
let cleanCss = require('gulp-clean-css');
let uglify = require('gulp-uglify');
let babel = require('gulp-babel');
let pump = require('pump');
let rename = require('gulp-rename');
let imagemin = require('gulp-imagemin');
let cache = require('gulp-cache');
let browserSync = require('browser-sync').create();
let phpConnect = require('gulp-connect-php');

let sourcePathCss = "dev-assets/scss/*.scss";
let destPathCss = "web/css/";

let sourcePathJs = "dev-assets/js/*.js";
let destPathJs = "web/js/";

let sourcePathImg = "dev-assets/images/*";
let destPathImg = "web/images/";


// Workflow css

gulp.task('styles', function(cb) {
  pump([
      gulp.src(sourcePathCss),
      sass(),
      autoPrefixer('last 2 versions'),
      rename({
        suffix: '.min',
      }),
      cleanCss(),
      gulp.dest(destPathCss),
      browserSync.stream()
    ],
    cb
  );
});

// Workflow javascript

gulp.task('scripts', function(cb) {
  pump([
      gulp.src(sourcePathJs),
      babel({
        presets: ['env']
      }),
      rename({
        suffix: '.min'
      }),
      uglify(),
      gulp.dest(destPathJs),
      browserSync.stream()
    ],
    cb
  );
});


// Workflow images

gulp.task('images', function() {
  gulp.src(sourcePathImg)
    .pipe(cache(imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest(destPathImg));
});

// Workflow PHP

gulp.task('php', function() {
  phpConnect.server({
    base: 'web/',
    port: 8010,
    keepalive: true
  });
  
});


// Finalisation du worflow et mise en place des écouteurs de modifications



// Static server
gulp.task('serve', ['styles', 'scripts', 'php'], function() {
    browserSync.init({
        proxy: "localhost:8010",
        port: 8080
    });
    gulp.watch(sourcePathCss, ['styles']);
    gulp.watch(sourcePathJs, ['scripts']);    
    gulp.watch("**/*.php").on('change', browserSync.reload);
});

// lancement de la tâche par defaut
gulp.task('default', ['serve']);
