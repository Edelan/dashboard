<?php

$config = new \Doctrine\DBAL\Configuration();

// Doctrine (db)
$dbParams = [
    'driver'   => 'pdo_mysql',
    'charset'  => 'utf8',
    'host'     => 'localhost',    
    'dbname'   => 'dashboard',
    'user'     => 'root',
    'password' => ''
   ];

$conn = Doctrine\DBAL\DriverManager::getConnection($dbParams, $config);