<?php

use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use App\DAO\UserDAO;

$app['debug'] = true;

// paramètre de la base
$app['db.options'] = [
    'driver' => 'pdo_mysql',
    'charset' => 'utf8',
    'host' => 'localhost',
    'port' => '3306',
    'dbname' => 'dashboard',
    'user' => 'root',
    'password' => ''
];

// Enregistrement de service provider (db.options)
$app->register(new Silex\Provider\DoctrineServiceProvider());

// 
$app->register(new TwigServiceProvider(), array(
    'twig.path' => __DIR__ . DIRECTORY_SEPARATOR . './../views', // http://php.net/manual/fr/function.dirname.php
    'twig.options' => array('debug', true),
));



$app->register(new AssetServiceProvider(), array(
    'assets.version' => 'v1',
    'assets.version_format' => '%s?version=%s',
    'assets.named_packages' => array(
        'css' => array('version' => 'css2',
            'base_path' => __DIR__ . '/web/css/'),
    ),
));

$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\RememberMeServiceProvider());
$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'secured' => array(
            'pattern' => '^/login',
            'anonymous' => true, //            
            'form' => array(
                'login_path' => '/login',
                'check_path' => '/login_check',
                'default_target_path' => '/dashboard',
            ),
            'users' => function () use ($app) {
                return new UserDAO($app['db']);
            }
        )
    ),
//    'security.access_rules' => array(
//    // ROLE_USER est défini arbitrairement, vous pouvez le remplacer par le nom que vous voulez
//        array('^/admin', 'ROLE_ADMIN')
//    )
));

//$app->register(new HttpFragmentServiceProvider()); // A quoi cela sert ??
// extensions pour utilisation dans twig
$app['twig'] = $app->extend('twig', function ($twig, $app) {
    $sidebarLinks = [
        ['path' => '/dashboard', 'icon' => 'fa-home', 'title' => 'Dashboard', 'color' => 'blue'],
        ['path' => '/stats', 'icon' => 'fa-line-chart', 'title' => 'Statistiques', 'color' => 'red'],
        ['path' => '/calendar', 'icon' => 'fa-calendar-o', 'title' => 'Calendrier', 'color' => 'green'],
        ['path' => '/mailbox', 'icon' => 'fa-envelope', 'title' => 'Messagerie', 'color' => 'brown'],
        ['path' => '/chat', 'icon' => 'fa-commenting', 'title' => 'Chat', 'color' => 'purple'],
        ['path' => '/user', 'icon' => 'fa-user', 'title' => 'Profile', 'color' => 'orange'],
    ];
    $sidebarLinksAdmin = [
        ['path' => '/admin/site', 'icon' => 'fa-sitemap', 'title' => 'Gestion du site', 'color' => 'blue'],
        ['path' => '/admin/users', 'icon' => 'fa-users', 'title' => 'Gestion des utilisateurs', 'color' => 'red'],
        ['path' => '/admin/database', 'icon' => 'fa-database', 'title' => 'Gestion des données', 'color' => 'yellow'],
    ];
    $sidebarLinksRh = [
        ['path' => '/rh/users', 'icon' => 'fa-users', 'title' => 'Gestion des salariés', 'color' => 'blue'],
        
    ];
    $sidebarLinksRhSettingsTitle = 'Gestion des Paramètres';
    $sidebarLinksRhSettings = [        
        ['path' => '/rh/service', 'icon' => 'fa-building-o', 'title' => 'Gestion des services', 'color' => 'red'],
        ['path' => '/rh/job', 'icon' => 'fa-suitcase', 'title' => 'Gestion des métiers', 'color' => 'green'],
        ['path' => '/rh/contract', 'icon' => 'fa-file-text-o', 'title' => 'Gestion des contrats', 'color' => 'brown'],
    ];
    $siteTitle = 'Dashboard Pro';
    $twig->addGlobal('sidebarLinks', $sidebarLinks);
    $twig->addGlobal('sidebarLinksAdmin', $sidebarLinksAdmin);
    $twig->addGlobal('sidebarLinksRh', $sidebarLinksRh);
    $twig->addGlobal('siteTitle', $siteTitle);
    $twig->addGlobal('sidebarLinksRhSettingsTitle', $sidebarLinksRhSettingsTitle);
    $twig->addGlobal('sidebarLinksRhSettings', $sidebarLinksRhSettings);
    return $twig;
});
//mise en place d'une barre de debug sur navigateur
// $app->register(new Dongww\Silex\Provider\DebugBarServiceProvider());
// acces base de données

$app['dao.user'] = function ($app) {
    return new \App\DAO\UserDAO($app['db']);
};

$app['dao.contact'] = function ($app) {
    return new \App\DAO\ContactDAO($app['db']);
};
$app['dao.database'] = function ($app) {
    return new \App\DAO\DataBaseDao($app['db']);
};
$app['dao.calendar'] = function ($app) {
    return new \App\DAO\CalendarDao($app['db']);
};
$app['dao.todolist'] = function ($app) {
    return new \App\DAO\TodoListDao($app['db']);
};
$app['dao.userSettings'] = function($app) {
    return new \App\DAO\UserSettingsDao($app['db']);
};

return $app;
