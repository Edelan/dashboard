<?php

// Définition des routes

$app->get('/', function () use ($app) {
    return $app->redirect('/login');
});
// route login, enregistrement et reset mdp
$app->get('/login', "App\Controller\AccessController::loginAction")
        ->bind('login');

$app->post('/login', "App\Controller\AccessController::loginUserAction")
        ->bind('loginUser');

$app->get('/register', "App\Controller\AccessController::registerAction")
        ->bind('register');

$app->post('/register', "App\Controller\AccessController::registerUserAction")
        ->bind('registerUser');

$app->post('/reset', "App\Controller\AccessController::resetUserAction")
        ->bind('reset');

$app->get('/logout', "App\Controller\AccessController::logoutUserAction")
        ->bind('logout');

$app->get('/page404', "App\Controller\AccessController::page404Action")
        ->bind('page404');

// route communes
$app->get('/dashboard', "App\Controller\DashboardController::dashboardAction")
        ->bind('dashboard');

$app->get('/stats', "App\Controller\StatsController::statsAction")
        ->bind('stats');

$app->get('/calendar', "App\Controller\CalendarController::calendarAction")
        ->bind('calendar');

$app->post('/calendar/create', "App\Controller\CalendarController::calendarAddEvent")
        ->bind('calendar-create');

$app->post('/calendar/update', "App\Controller\CalendarController::calendarUpdateEvent")
        ->bind('calendar-update');

$app->post('/calendar/delete', "App\Controller\CalendarController::calendarDeleteEvent");

$app->get('/mailbox', "App\Controller\MailboxController::MailboxAction")
        ->bind('mailbox');

$app->get('/chat', "App\Controller\ChatController::chatAction")
        ->bind('chat');

// routes user management CRUD
$app->get('/user', "App\Controller\UserController::userGetAll")
        ->bind('user');

$app->get('/user/{id}', "App\Controller\UserController::userGetId");

$app->post('/user/create', "App\Controller\UserController::userCreate")
        ->bind('user-create');

$app->post('/user/update', "App\Controller\UserController::userUpdate");


// routes pour les manager

$app->get('/rh/users', "App\Controller\UserController::rhUsersAction")
        ->bind('rh-users');

$app->get('/rh/user/display', "App\Controller\UserController::UserDisplayById")
        ->bind('rh-user-display');

$app->get('/rh/user/display/{id}', "App\Controller\UserController::UserDisplayGetId");
        

$app->post('/rh/user/update/{id}', "App\Controller\UserController::userUpdate")
        ->bind('rh-user-update');

$app->get('/rh/service', "App\Controller\UserSettingsController::settingsServiceAction")
        ->bind('rh-service');

$app->post('/rh/service/create', "App\Controller\UserSettingsController::settingsServiceCreate")
        ->bind('rh-service-create');

$app->post('/rh/service/update', "App\Controller\UserSettingsController::settingsServiceUpdate");
        

$app->post('/rh/service/delete', "App\Controller\UserSettingsController::settingsServiceDelete");
        

$app->get('/rh/job', "App\Controller\UserSettingsController::settingsJobAction")
        ->bind('rh-job');

$app->post('/rh/job/create', "App\Controller\UserSettingsController::settingsJobCreate")
        ->bind('rh-job-create');

$app->post('/rh/job/update', "App\Controller\UserSettingsController::settingsJobUpdate");
        

$app->post('/rh/job/delete', "App\Controller\UserSettingsController::settingsJobDelete");

$app->get('/rh/contract', "App\Controller\UserSettingsController::settingsContractAction")
        ->bind('rh-contract');

$app->post('/rh/contract/create', "App\Controller\UserSettingsController::settingsContractCreate")
        ->bind('rh-contract-create');

$app->post('/rh/contract/update', "App\Controller\UserSettingsController::settingsContractUpdate");
        

$app->post('/rh/contract/delete', "App\Controller\UserSettingsController::settingsContractDelete");


// routes pour les todolist

// annulation car todolist uniquement dans dashboard.
//$app->get('/todolist', "App\Controller\TodoListController::TodoListAction")
//        ->bind('todolist');

$app->post('/todolist/create', "App\Controller\TodoListController::TodoListAddEvent");

$app->post('/todolist/update', "App\Controller\TodoListController::TodoListAddEvent");

$app->post('/todolist/reorder', "App\Controller\TodoListController::reorderTodo");

$app->post('/todolist/delete', "App\Controller\TodoListController::TodoListDeleteEvent");        

// routes pour l'administrateur
$app->get('/admin', "App\Controller\AdminController::loginAdmin")
        ->bind('admin');

$app->post('/admin/login', "App\Controller\AdminController::loginAdminAction");

$app->get('/admin/dashboard', "App\Controller\AdminController::dashboardAdminAction")
        ->bind('admin-dashboard');

$app->get('/admin/user/{id}', "App\Controller\AdminController::adminGetUserId");

$app->post('/admin/user/update/{id}', "App\Controller\AdminController::adminUpdateUserById")
        ->bind('admin-update-user');

// cette route n'est pas utilisée car il n'y a pas de suppression d'utilisateur, le statut change
//$app->get('/admin/user/delete/{id}', "App\Controller\AdminController::adminUpdateUserById");

$app->post('/admin/user/create', "App\Controller\AdminController::adminCreateUser")
        ->bind('admin-create-user');

$app->get('/admin/users', "App\Controller\AdminController::usersAdminAction")
        ->bind('admin/users');

$app->get('/admin/database', "App\Controller\DataBaseController::sizeDataBase")
        ->bind('admin/database');

$app->get('/admin/database/backup', "App\Controller\DataBaseController::backupDataBase");
